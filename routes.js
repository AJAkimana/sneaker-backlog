/**
 * Different Controllers (route handlers).
 */
const productController = require('./controllers/productCtrl');
const infoController = require('./controllers/infoCtrl');
const userController = require('./controllers/userCtrl');

module.exports = function(app) {
	// ---Display every request on the server
	app.use((req,res,next) =>{
		console.log("Request Method: "+ req.method+" @ "+JSON.stringify(req.path))
		console.log('About Session:'+JSON.stringify(req.session))
		return next();
	});
	var userCarts =function(req,res,next){		
		if(!req.session.carts) req.session.carts =[];
		return next();
	}
	var isAuthenticated =function(req,res,next){
		if(req.isAuthenticated() ||req.query.token) return next();
		return res.redirect('/user.signin');     	
	};

	app.use(userCarts);
	app.get('/', infoController.goToMainPage);
	app.get('/products/:page_no', infoController.getMainPage);
	app.get('/product.one/:product_id', productController.getOneProductPage);
	app.get('/products.json/:page_no/:page_size', productController.getJson);
	app.get('/product/:product_id', productController.getOneProduct);
	app.post('/add.to.cart', productController.addToCart);
	app.get('/my.cart', productController.getMyCartPage);
	app.get('/cart.json/:user', productController.getCartJson);
	app.get('/checkout', productController.checkoutPage);
	app.get('/action.checkout', productController.postCheckout);
	app.get('/img/:prod_id', productController.getImage);
	//If requested route not found, get page not faound msg
	app.all('*',infoController.getPage404);
}
