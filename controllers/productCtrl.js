const Json = require('../config/jsonReader');

exports.getJson = (req, res)=>{
  req.assert('page_no','Invalid page number').isInt();
  req.assert('page_size','Invalid page size').isInt();
  const errors = req.validationErrors();
  if (errors) return res.status(400).send(errors[0].msg);
  var prodList = [],list={};
  var pageNo = req.params.page_no,
      pageSize = req.params.page_size,
      skip = pageSize * (pageNo-1);
  var async = require('async');
  Json.readJSONFile((err,data)=>{
    if(err) return res.status(500).send('Service not available:'+err);
    var totalPage = data.length;
    var remainder = data.length%pageSize==0?0:1;
    var pages = Math.floor(data.length/pageSize)+remainder;
    console.log(('pages:'+pages))
    async.eachSeries(data,(theProd,prodCb)=>{
      // console.log(('id:'+theProd.id))
      if(theProd.id>skip&&theProd.id<=(pageNo*pageSize)){
        prodList.push(theProd);
      }
      prodCb();
    })
    list.products = prodList;
    list.pages = pages;
    return res.json(list)
  })
}
exports.getOneProduct = (req, res)=>{
  req.assert('product_id','Invalid product').isInt();
  const errors = req.validationErrors();
  if (errors) return res.status(400).send(errors[0].msg)
  var prodId = req.params.product_id;
  Json.findById(prodId,(err,data)=>{
    if(err) return res.status(500).send('Service not available:'+err);
    res.json(data);
  })
}
exports.addToCart = (req, res)=>{
  req.assert('prod_id','Invalid product').isInt();
  req.assert('user','User is empty').notEmpty();
  const errors = req.validationErrors();
  if (errors) return res.status(400).send(errors[0].msg)
  console.log('Data:'+JSON.stringify(req.body))
  var currentCart = req.session.carts;
  var prod_index=currentCart.findIndex(x=>x.product_id==req.body.prod_id);
  
  if(prod_index!=-1){
    return res.status(400).send("Sorry the product has already added on list");
  }
  req.session.carts.push({product_id:req.body.prod_id,username:req.body.user});
  return res.end();
}
exports.getCartJson = (req, res)=>{
  req.assert('user','User is empty').notEmpty();
  const errors = req.validationErrors();
  if (errors) return res.status(400).send(errors[0].msg);
  var myCarts=[];
  var async = require('async');
  var allCarts = req.session.carts;
  // console.log('Carts:')
  async.eachSeries(allCarts, (thisProd, prodCb)=>{
    if(thisProd.username==req.params.user){
      Json.findById(thisProd.product_id,(err, prod_info)=>{
        if(err) return prodCb(err);
        console.log('Carts:'+JSON.stringify(prod_info))
        myCarts.push(prod_info[0]);
        prodCb(null);
      })
    }
  },(err)=>{
    if(err) return res.status(400).send("Error occured");
    return res.json(myCarts);
  })
}
exports.getOneProductPage = (req, res)=>{
  req.assert('product_id','Invalid product').isInt();
  const errors = req.validationErrors();
  if (errors) return res.render("./lost",{msg:errors[0].msg});
  Json.findById(req.params.product_id,(err,data)=>{
    if(err) return res.render("./lost",{msg:'Service not available:'+err});
    return res.render('info/view_product',{
      title:data[0].name,
      prod_id:req.params.product_id,
    });
  })
}
exports.getMyCartPage = (req, res)=>{
  return res.render('info/cart',{
    title: 'My cart'
  });
}
exports.checkoutPage = (req, res)=>{
  return res.render('info/checkout.pug',{
    title: 'Check out page'
  });
}
exports.postCheckout = (req, res)=>{
  req.session.carts=[];
  res.end();
}
exports.getImage = (req, res)=>{
  req.assert('prod_id','Invalid product').isInt();
  const errors = req.validationErrors();
  var path = require('path'),
      fs=require("fs");
  var prodId = req.params.prod_id>11?req.params.prod_id%11:req.params.prod_id;
  var pct_loc=path.join(__dirname, '../Image_folder/');
  if(errors) return res.sendFile(pct_loc+"/default.png");
  
  var file_location=pct_loc+"/"+prodId+".png";
  fs.access(file_location, fs.constants.F_OK | fs.constants.R_O,(err)=>{
    if(err) {
      file_location=pct_loc+"/default.png";
    }
    return res.sendFile(file_location);  
  });
}