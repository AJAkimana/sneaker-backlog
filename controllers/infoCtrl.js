const Json = require('../config/jsonReader');
exports.getMainPage = (req, res) => {
	req.assert('page_no','Invalid page number').isInt();
	req.assert('page_size','Invalid page size').isInt();
	const errors = req.validationErrors();
	if (errors) return res.status(400).send(errors[0].msg);
	return res.render('info/homepage',{
		title: 'Blacklog for Sneaker',
		page_no: req.params.page_no,
		page_size: req.query.page_size,
	});
}
exports.goToMainPage = (req, res)=>{
	return res.redirect('/products/1?page_size=10')
}
exports.getPage404 = (req, res) => {
	return res.render('page404',{
		title: 'Page not found',
	});
}