# Backlog for Sneaker City

Sneaker City is a brand new online-only store for sneaker enthusiasts.

## Technologies

  * Runtime environment: [Node](https://nodejs.org/)
  * Backend framework: [Express.js](https://expressjs.com/)
  * Frontend Javascript framework: [Angularjs](https://angularjs.org/)
  * View engine: [Pug](https://pugjs.org)

## Installation

Before installing, [download and install Node.js](https://nodejs.org/en/download/).

## Features

  * View latest sneakers
  * View availability of sneaker size
  * Add sneaker to cart
  * Review cart and checkout

## Quick Start

  The quickest way to setup Sneaker app is as shown below:

```
git clone https://gitlab.com/AJAkimana/sneaker-backlog.git
cd sneaker-backlog
```

  Install dependencies:

```bash
$ npm install
```

  Start the server:

```bash
$ nodemon app.js
```

## The current maintainer:

[Akimana Jean D'Amour](https://gitlab.com/AJAkimana)

