var fs = require('fs'),
    filename = __dirname+'/mocked_products.json';

exports.readJSONFile=(callback)=>{
  fs.readFile(filename, (err, data)=>{
    if(err) {
      callback(err);
      return;
    }
    try {
      callback(null, JSON.parse(data));
    } catch(exception) {
      callback(exception);
    }
  });
}
exports.findById=(prodId, callback)=>{
  fs.readFile(filename, (err, data)=>{
    if(err) return callback(err);
    try {
      let jsonData = JSON.parse(data);
      var productsjson = jsonData[2].data;

      var product = jsonData.filter((value)=>{ 
        // console.log('Json:'+JSON.stringify(value.id))
        return value.id==prodId;
      })
      callback(null, product);
    } catch(exception) {
      callback(exception);
    }
  });
}