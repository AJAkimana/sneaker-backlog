/**
 * Module dependencies.
 */
const express = require('express');
const compression = require('compression');
const session = require('express-session');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const errorHandler = require('errorhandler');
const expressValidator = require('express-validator');
const path = require('path');

/**
 * Create Express server.
 */
const app = express();
/**
 * Express configuration.
 */
app.set('port', 8080);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var hours = 3600000;
var weeks = 7 * 24 * hours;
app.use(cookieParser());

app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: "MySecretSessionId",
  name : 'PHPSESSID',     // simulate Php cookie
  //When still not connected the cookie will live 2 hours
  cookie:{ path: '/', httpOnly: true, secure: false, maxAge: 2*hours },
}));
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 2*hours }));
app.use(expressValidator({
   customValidators: {
    isArray: function(value) {
        return Array.isArray(value);
    },
    gte: function(param, num) {
        return param >= num;
    },
    hasNoWhiteSpace:function(value) {
        return value.indexOf(' ') ==-1;
    },
   }
}));

require('./routes')(app);
/*
 * Error Handler.
 */
app.use(errorHandler());
/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log('Sneaker app is listening on port %d. Visit http://localhost:%d', 
  app.get('port'),app.get('port'));
});
module.exports = app;
